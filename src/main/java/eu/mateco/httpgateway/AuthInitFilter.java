package eu.mateco.httpgateway;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

@Component
public class AuthInitFilter extends ZuulFilter {

  private final AuthServer authServer;

  public AuthInitFilter(AuthServer authServer) {
    this.authServer = authServer;
  }

  @Override
  public String filterType() {
    return "post";
  }

  @Override
  public int filterOrder() {
    return 1;
  }

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() throws ZuulException {
    try {
      RequestContext ctx = RequestContext.getCurrentContext();
      HttpServletResponse response = ctx.getResponse();
      if (response.getStatus() == 401) {
        response.sendRedirect(this.authServer.getLoginUrl(ctx.getRequest()));
      }
      return null;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
