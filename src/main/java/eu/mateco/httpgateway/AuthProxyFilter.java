package eu.mateco.httpgateway;

import com.auth0.SessionUtils;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.stereotype.Component;

@Component
public class AuthProxyFilter extends ZuulFilter {

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 1;
  }

  @Override
  public boolean shouldFilter() {
    return true;
  }

  @Override
  public Object run() throws ZuulException {
    final String idToken = (String) SessionUtils.get(RequestContext.getCurrentContext().getRequest(), "idToken");
    if (idToken != null) {
      RequestContext.getCurrentContext().addZuulRequestHeader("X-User-Id", idToken);
    }
    return null;
  }
}
