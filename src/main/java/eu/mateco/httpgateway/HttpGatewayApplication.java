package eu.mateco.httpgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class HttpGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HttpGatewayApplication.class, args);
	}

}
