package eu.mateco.httpgateway;

import com.auth0.IdentityVerificationException;
import com.auth0.SessionUtils;
import com.auth0.Tokens;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import java.io.IOException;
import org.springframework.stereotype.Component;

@Component
public class AuthCallbackFilter extends ZuulFilter {

  private final AuthServer authServer;

  public AuthCallbackFilter(AuthServer authServer) {
    this.authServer = authServer;
  }

  @Override
  public String filterType() {
    return "pre";
  }

  @Override
  public int filterOrder() {
    return 0;
  }

  @Override
  public boolean shouldFilter() {
    RequestContext ctx = RequestContext.getCurrentContext();
    return ctx.getRequest().getParameter("code") != null && ctx.getRequest().getRequestURI().equals("/loginCallback");
  }

  @Override
  public Object run() throws ZuulException {
    RequestContext ctx = RequestContext.getCurrentContext();
    Tokens tokens = null;
    try {
      tokens = this.authServer.validate(ctx.getRequest());
      SessionUtils.set(ctx.getRequest(), "accessToken", tokens.getAccessToken());
      SessionUtils.set(ctx.getRequest(), "idToken", tokens.getIdToken());
      ctx.getResponse().sendRedirect("/hello-user");
    } catch (IdentityVerificationException e) {
      ctx.getResponse().setStatus(403);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return null;
  }
}
