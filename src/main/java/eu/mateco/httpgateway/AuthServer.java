package eu.mateco.httpgateway;

import com.auth0.AuthenticationController;
import com.auth0.IdentityVerificationException;
import com.auth0.Tokens;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

@Component
public class AuthServer {

  private final AuthenticationController controller;

  public AuthServer() {
    this.controller = AuthenticationController.newBuilder(
        "__AUTH0_DOMAIN_HERE__",
        "__CLIENT_ID_HERE__",
        "__SECRET_HERE__").build();
  }

  public String getLoginUrl(final HttpServletRequest request) {
    return this.controller.buildAuthorizeUrl(request, "http://localhost:8080/loginCallback")
        .withAudience(String.format("https://%s/userinfo", "__AUTH0_DOMAIN_HERE__"))
        .withScope("openid profile email")
        .build();
  }

  public Tokens validate(final HttpServletRequest request) throws IdentityVerificationException {
    return this.controller.handle(request);
  }
}
